(defsystem #:metronome
  :version "0.0.1"
  :description "A simple metronome."
  :author "Cameron Chaparro <cameron@chaparro.io>"
  :serial t
  :components ((:file "metronome")))
