(defpackage #:io.chaparro.metronome
  (:nicknames #:icm #:ic-metronome #:metronome)
  (:use #:cl))

(in-package #:io.chaparro.metronome)
